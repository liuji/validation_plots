# validation_plots

do validations, read information from TRUTH0 derivations

main codes from Avik's git https://gitlab.cern.ch/avroy/MG_Reweight_testinATLAS2/-/tree/new_MCWorkFlow

## How to generate samples from JOs?

```
setupATLAS
asetup 21.6.21,AthGeneration,here

Gen_tf.py --ecmEnergy=13000.0 --randomSeed=1234 --jobConfig=mc.MGPy8EG_ZBHb1000LH100_sigonly.py --outputEVNTFile=EVNT.root --maxEvents=20000
```

## How to do truth derivation?
```
setupATLAS
asetup 21.2.86.0,AthDerivation,here

inputEVNTFile="EVNT.root"
outputDAODFile="DAOD.root"
outputDerivation="TRUTH0" #could be TRUTH3
Reco_tf.py --inputEVNTFile ${inputEVNTFile} --outputDAODFile ${outputDAODFile} --reductionConf ${outputDerivation}
```
## How to do check the DxAOD?
```
mkdir DAOD_tester && cd DAOD_tester
cp ../asetup_top.sh ../readxAOD.py ../DAODreader.py .
source asetup_top.sh
ln -s ../Test_DER*/DAOD* ## create symlink for reco DAOD
ln -s ../Test_TRUTH*/DAOD* ## create symlink for truth DAOD
python DAODreader.py
	
#In asetup_top.sh
setupATLAS
asetup AnalysisTop,21.2.49
```

You can use the readxAOD.py script to to create a transient tree from the DAOD. Use it to navigate the contents and make plots. 